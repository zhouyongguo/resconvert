﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResManager
{
    public class Res
    {
        private static System.Resources.ResourceManager resourceMan;
        public static string GetStr(string key)
        {
             string str=ResourceManager.GetString(key);
             if (string.IsNullOrEmpty(str))
                 return key;
             else
                 return str;
        }

        internal static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    System.Resources.ResourceManager temp = new System.Resources.ResourceManager("Resources.Res",System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
    }
}
