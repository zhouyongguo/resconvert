﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lab.Text = "hi，你好，world";
        }
        protected void ButCn_Click(object sender, EventArgs e)
        {
            CultureInfo ci = new CultureInfo("zh-cn");
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            lab.Text = "你好，世界";
        }
        protected void ButTw_Click(object sender, EventArgs e)
        {
            CultureInfo ci = new CultureInfo("zh-hant");
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            lab.Text = "你好，世界";
        }
        protected void ButEn_Click(object sender, EventArgs e)
        {
            CultureInfo ci = new CultureInfo("en");
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            lab.Text = "你好，世界";
        }
    }
}