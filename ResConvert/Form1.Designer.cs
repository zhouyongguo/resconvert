﻿namespace LangApp
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.butSearch = new System.Windows.Forms.Button();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.textdir = new System.Windows.Forms.TextBox();
            this.butDir = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.cheAutoTranslate = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // butSearch
            // 
            this.butSearch.Location = new System.Drawing.Point(47, 337);
            this.butSearch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.butSearch.Name = "butSearch";
            this.butSearch.Size = new System.Drawing.Size(148, 27);
            this.butSearch.TabIndex = 0;
            this.butSearch.Text = "扫描项目";
            this.butSearch.UseVisualStyleBackColor = true;
            this.butSearch.Click += new System.EventHandler(this.butSearch_Click);
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(47, 43);
            this.txtLog.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtLog.Name = "txtLog";
            this.txtLog.Size = new System.Drawing.Size(683, 275);
            this.txtLog.TabIndex = 1;
            this.txtLog.Text = "";
            // 
            // textdir
            // 
            this.textdir.Location = new System.Drawing.Point(47, 12);
            this.textdir.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textdir.Name = "textdir";
            this.textdir.Size = new System.Drawing.Size(583, 25);
            this.textdir.TabIndex = 2;
            // 
            // butDir
            // 
            this.butDir.Location = new System.Drawing.Point(638, 10);
            this.butDir.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.butDir.Name = "butDir";
            this.butDir.Size = new System.Drawing.Size(92, 27);
            this.butDir.TabIndex = 3;
            this.butDir.Text = "选择项目";
            this.butDir.UseVisualStyleBackColor = true;
            this.butDir.Click += new System.EventHandler(this.butDir_Click);
            // 
            // cheAutoTranslate
            // 
            this.cheAutoTranslate.AutoSize = true;
            this.cheAutoTranslate.Location = new System.Drawing.Point(528, 337);
            this.cheAutoTranslate.Name = "cheAutoTranslate";
            this.cheAutoTranslate.Size = new System.Drawing.Size(119, 19);
            this.cheAutoTranslate.TabIndex = 4;
            this.cheAutoTranslate.Text = "自动英文翻译";
            this.cheAutoTranslate.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 389);
            this.Controls.Add(this.cheAutoTranslate);
            this.Controls.Add(this.butDir);
            this.Controls.Add(this.textdir);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.butSearch);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form1";
            this.Text = "多语言扫描工具";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button butSearch;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.TextBox textdir;
        private System.Windows.Forms.Button butDir;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox cheAutoTranslate;
    }
}

