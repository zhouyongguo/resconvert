﻿using LangApp.Api;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LangApp
{
    public partial class Form1 : Form
    {
        AppConfig config = null;
        Dictionary<string, int> words = new Dictionary<string, int>();
        public Form1()
        {
            InitializeComponent();
        }

        private void butDir_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.textdir.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void butSearch_Click(object sender, EventArgs e)
        {
          //this.textdir.Text = "C:\\code\\tools\\WebApp";
         // this.textdir.Text = "C:\\code\\urp\\webregister";
            config = AppConfig.CreateConfig();
            config.AutoTranslate = this.cheAutoTranslate.Checked;
            config.ProjectDir = this.textdir.Text;

            run();

        }
        public void run()
        {
            if (System.IO.Directory.Exists(config.ProjectDir) == false)
            {
                this.txtLog.Text += "项目路径不存在！";
                return;
            }
            try
            {
                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(config.ProjectDir);
                ListDir(dir);
            }
            catch (System.Exception ex)
            {
                this.txtLog.Text += "\n" + ex.Message;
            }


            // 生成资源文件
         
            try
            {  
                this.txtLog.Text += "正在 生成资源文件 \n";
                ResManage manage = new ResManage();
                manage.Save();
                this.txtLog.Text += "资源生成完成！";

            }
            catch (System.Exception ex)
            {
                this.txtLog.Text += "\n" + ex.Message;
            }


        }

        void ListDir(System.IO.DirectoryInfo parentDir)
        {
            foreach (var file in parentDir.GetFiles())
            {
                this.txtLog.Text += "正在扫描文件：" + file.FullName + "\n";
                var tep = FileFactory.CreateFile(file.FullName);
                if (tep != null)
                    tep.FileSave();
            }
            foreach (var dir in parentDir.GetDirectories())
            {
                if (config.ValidateDir(dir.Name))
                    ListDir(dir);
            }
        }


    }
}
