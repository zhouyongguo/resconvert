﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api.Con
{
    public class AspxCodeCon : ConBase
    {
        string reg = "<%(.)+?%>";
        string reg1 = "(\\\")([^\\\"]*)(\\\")";
        string reg2 = "[\\u4e00-\\u9fa5]+";
        public override string ModifyCode(string str, Dictionary<string, string> lineDict)
        {
            var codes = this.Find(reg, str);
            foreach (string code in codes)
            {
                string codeStr = code;
                var texts = this.Find(reg1, codeStr);
                foreach (string text in texts)
                {
                    string textStr = text;
                    var keyStrs = this.Find(reg2, textStr);
                    foreach (string keyStr in keyStrs)
                    {
                        string key = this.CreateKey(keyStr);
                        if (string.IsNullOrEmpty(key) == false)
                        {
                            config.AddWord(key, keyStr);
                            textStr = textStr.Replace(keyStr, "\"+ResManager.Res.GetStr(\"" + key + "\")+\"");
                        }
                    }
                    if (keyStrs.Count > 0)
                    {
                        string uid = "[" + System.Guid.NewGuid().ToString() + "]";
                        lineDict.Add(uid, textStr);
                        codeStr = codeStr.Replace(text, uid);
                    }
                }
                str = str.Replace(code, codeStr);
            }
            return str;
        }

    }
}
