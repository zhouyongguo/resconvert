﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api.Con
{

    public class AspxTextCon : ConBase
    {
        string reg = "(ErrorMessage|Text|HeaderText|OnClientClick)=\\\"[^\\\"]*\\\"";
        string reg1 = "(\\\")([^\\\"]*)(\\\")";

        public override string ModifyCode(string str, Dictionary<string, string> lineDict)
        {
            var texts = this.Find(reg, str);
            foreach (string text in texts)
            {
                string tepStr = text;
                var strs = this.Find(reg1, text);
                if (strs.Count == 1)
                {
                    var keyStr = strs[0].Substring(1, strs[0].Length - 2);
                    if (this.isCnStr(keyStr))
                    {
                        string key = CreateKey(keyStr);
                        if (string.IsNullOrEmpty(key) == false)
                        {
                            string uid = "[" + System.Guid.NewGuid().ToString() + "]";
                            tepStr = tepStr.Replace(keyStr, "<%$Resources:Res," + key + "%>");
                            config.AddWord(key, keyStr);
                            lineDict.Add(uid, tepStr);
                            str = str.Replace(text, uid);
                        }
                    }
                }
            }
            return str;
        }
    }
}