﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LangApp.Api.Con
{
    public class HtmlStrCon : ConBase
    {
        string reg = "([\\u4e00-\\u9fa5]|[，；？、])+";
        public override string ModifyCode(string str, Dictionary<string, string> lineDict)
        {
            var texts = this.Find(reg, str);
            foreach (string text in texts)
            {
                string key = this.CreateKey(text);
                if (string.IsNullOrEmpty(key) == false)
                {
                    string uid = "[" + System.Guid.NewGuid().ToString() + "]";
                    str = str.Replace(text, uid);
                    lineDict.Add(uid, "<% =ResManager.Res.GetStr(\"" + key + "\") %>");
                    config.AddWord(key,text);
                }
            }
            return str;
        }
    }
}

