﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api.Con
{
    public class CsStrsCon : ConBase
    {
        string reg = "(@\\\")([^\\\"]*)(\\\")";
        string reg1 = "[\\u4e00-\\u9fa5]+";
        public override string ModifyCode(string str, Dictionary<string, string> lineDict)
        {
            var texts = this.Find(reg, str);
            foreach (string text in texts)
            {
                var keyStrs = this.Find(reg1, text);
                if (keyStrs.Count > 0)
                {  
                    config.StrNumber++;
                    string key = "Str" + config.StrNumber.ToString();
                    string uid = "[" + System.Guid.NewGuid().ToString() + "]";
                    str = str.Replace(text, uid);
                    lineDict.Add(uid, "ResManager.Res.GetStr(\"" + key + "\")");
                    config.AddWord(key, text.Substring(1, text.Length - 3));
                }
            }
            return str;
        }


    }
}
