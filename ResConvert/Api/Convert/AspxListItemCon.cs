﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api.Con
{
    public class ListItemCon : ConBase
    {
        string reg = "<asp:ListItem(.*?)</asp:ListItem>";
        string reg1 = ">.+<";
        string reg2 = "(\\\")([^\\\"]*)(\\\")";
       
        public override string ModifyCode(string str, Dictionary<string, string> lineDict)
        {
            var texts = this.Find(reg, str);
            foreach (string text in texts)
            {
                string tepStr = text;
                var keyStrs = this.Find(reg1, tepStr);
                if (keyStrs.Count == 1)
                {
                    var keyStr = keyStrs[0].Substring(1, keyStrs[0].Length - 2);
                    if (this.isCnStr(keyStr))
                     {
                         tepStr= tepStr.Replace(keyStrs[0], " Text=\"" + keyStr + "\"><");
                     }
                }

                keyStrs = this.Find(reg2, tepStr);
                foreach (string keyStr in keyStrs)
                {
                    var skey = keyStr.Substring(1, keyStr.Length - 2);
                    if (this.isCnStr(skey))
                    {
                        string key = this.CreateKey(skey);
                        if (string.IsNullOrEmpty(key) == false)
                        {
                            string uid = "[" + System.Guid.NewGuid().ToString() + "]";
                            lineDict.Add(uid, "<%$Resources:Res," + key + "%>");
                            tepStr = tepStr.Replace(skey, uid);
                            config.AddWord(key, skey);
                        }
                    }
                }
                str = str.Replace(text, tepStr);
            }
            return str;
        }
    }
}
