﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LangApp.Api.Con
{
    public abstract class ConBase
    {
        public AppConfig config = AppConfig.CreateConfig();
       
        public List<string> Find(string reg, string context)
        {
            List<string> list = new List<string>();
            RegexOptions option = RegexOptions.IgnoreCase | RegexOptions.Singleline;
            Regex regex = new Regex(reg, option);
            MatchCollection matchs = regex.Matches(context);
            foreach (Match match in matchs)
            {
                list.Add(match.Value);
            }

            return list;
        }
        public bool isCnChar(char cha)
        {
            if (cha == '”' || cha == '“' || cha == '”' || cha == '，' || cha == '。' || cha == '！' || cha == '；')
                return true;
            int currentcode = (int)cha;
            if (currentcode > 19968 && currentcode < 40869)
                return true;
            else
                return false;
        }
        public bool isCnStr(string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;


            for (int i = 0; i < str.Length; i++)
            {
                int currentcode = (int)str[i];
                if (currentcode > 19968 && currentcode < 40869)
                    return true;
            }

            return false;
        }
        public string CreateKey(string keyStr)
        {
            string[] strs = new string[] { "\n", "/n", "<br>", "<br/>", "\\", "/", "!", "=", "＊", "nbsp;", ";", "？", "！", "。", "：",":", "'", "<", ">", "(", ")", "&", "*", " ", ",", ".", "?", "】", "【", "　", "；", "、", "，" };
            char[] strs1 = new char[] { '\n', '\r', '-'};
            string[] strs2 = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            foreach(string str in strs)
            {
                keyStr = keyStr.Replace(str,"");
            }
            foreach (char str in strs1)
            {
                keyStr = keyStr.Replace(str, '_');
            }
            foreach (string str in strs2)
            {
                keyStr = keyStr.Replace(str, "");
            }
            return keyStr;
        }
        public abstract string ModifyCode(string str, Dictionary<string, string> lineDict);

    }
}
