﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api.Con
{
    public class CsStrCon : ConBase
    {
        string reg = "(\\\")([^\\\"]*)(\\\")";
        string reg1 = "[\\u4e00-\\u9fa5]+";
        public override string ModifyCode(string str, Dictionary<string, string> lineDict)
        {
            var texts = this.Find(reg, str);
            foreach (string text in texts)
            {
                string tepStr = text;
                var keyStrs = this.Find(reg1, tepStr);
                foreach (string keyStr in keyStrs)
                {
                    string key = this.CreateKey(keyStr);
                    if (string.IsNullOrEmpty(key) == false)
                    {
                        config.AddWord(key, keyStr);
                        tepStr = tepStr.Replace(keyStr, "\"+ResManager.Res.GetStr(\"" + key + "\")+\"");
                    }
                }
                if (keyStrs.Count > 0)
                {
                    string uid = "[" + System.Guid.NewGuid().ToString() + "]";
                    lineDict.Add(uid, tepStr);
                    str = str.Replace(text, uid);
                }
            }
            return str;
        }

       
    }
}
