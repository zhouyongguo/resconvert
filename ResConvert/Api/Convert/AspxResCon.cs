﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api.Con
{
    //忽略检索已经替换的资源
    public class AspxResCon : ConBase
    {   
        string reg =  "(<% =ResManager.Res.GetStr\\(\\\".*?\\\"\\) %>)|(<%\\$Resources:Res,.*?%>)";
      
        public override string ModifyCode(string str, Dictionary<string, string> lineDict)
        {
            var texts = this.Find(reg, str);
            foreach (string text in texts)
            {
                string uid = "[" + System.Guid.NewGuid().ToString() + "]";
                str = str.Replace(text, uid);
                lineDict.Add(uid, text);
            }
            return str;
        }
    }
}
