﻿
using LangApp.Api.Con;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api
{
    public class AspxFile : FileBase
    {
        Dictionary<string, string> lineDict = new Dictionary<string, string>();
        public AspxFile(string filePath)
        {
            this.Path = filePath;
        }

        public override List<ConBase> GetCons()
        {
            List<ConBase> cons = new List<ConBase>();
            cons.Add(new AspxResCon());
            cons.Add(new CsResCon());
            
            cons.Add(new AspxCodeCon());//筛选html中嵌入代码
            cons.Add(new ListItemCon());
            cons.Add(new AspxTextCon());
            cons.Add(new AspxStrCon());
            return cons;
        }
        

    }
}
