﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api
{
    public class FileFactory
    {
        public static FileBase CreateFile(string filePath)
        {
            var ext = System.IO.Path.GetExtension(filePath).ToLower();
            if (ext == ".aspx" || ext == ".ascx" || ext==".master")
                return new AspxFile(filePath);
            else if (ext == ".cs")
                return new CsFile(filePath);
            else if (ext == ".cshtml")
                return new CshtmlFile(filePath);
            else
                return null;
        }
    }
}
