﻿using LangApp.Api.Con;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api
{
    public class CshtmlFile : FileBase
    {
        public CshtmlFile(string filePath)
        {
            this.Path = filePath;
        }
        public override List<ConBase> GetCons()
        {
            List<ConBase> cons = new List<ConBase>();
            cons.Add(new HtmlResCon());
            cons.Add(new HtmlStrCon());
            return cons;
        }
    }
}
