﻿using LangApp.Api.Con;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api
{
    public abstract class FileBase
    {

        public string Path { get; set; }
        private string ModifyCode()
        {
            string str =System.IO.File.ReadAllText(Path, Encoding.UTF8);
            Dictionary<string, string> lineDict = new Dictionary<string, string>();
            List<ConBase> cons = GetCons();
            foreach (ConBase con in cons)
            {
                str = con.ModifyCode(str, lineDict);
            }

            foreach (var key in lineDict.Keys)
            {
                str = str.Replace(key, lineDict[key]);
            }
            return str;
        }
        public void FileSave()
        {
            System.IO.File.WriteAllText(Path, ModifyCode(), Encoding.UTF8);
        }

        public abstract List<ConBase> GetCons();


    }
}
