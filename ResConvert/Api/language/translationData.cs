﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api
{
    public class translationData
    {
        public List<string> translation { get; set; }
        public string English()
        {
            if (translation.Count > 0)
            {
                return translation[0];
            }
            return "";

        }
        public static translationData Create(string json)
        {
            return Json.Deserialize<translationData>(json);
        }
    }
}
