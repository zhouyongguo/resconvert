﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Text;
using System.IO;
namespace LangApp.Api
{
    /// <summary>
    /// Json对象
    /// </summary>
    public class Json
    {
        static JavaScriptSerializer serializer = null;
        static JavaScriptSerializer CreateJavaScriptSerializer()
        {
            if (serializer == null)
            {
                serializer = new JavaScriptSerializer();               
            }
            return serializer;
        }
      
        /// <summary>
        /// 将对象转为Json字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string Serialize(object obj)
        {
            JavaScriptSerializer jss = CreateJavaScriptSerializer();          
            return jss.Serialize(obj);
        }
        /// <summary>
        /// 将对象序列化并将Json字符串写入指定的StringBuilder
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="output"></param>
        public static void Serialize(object obj, StringBuilder output)
        {
            JavaScriptSerializer jss = CreateJavaScriptSerializer();
            jss.Serialize(obj, output);
        }
        /// <summary>
        /// 将Json格式字符串转为指定类型的对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string input)
        {
            JavaScriptSerializer jss = CreateJavaScriptSerializer();
            return jss.Deserialize<T>(input);
        }
        /// <summary>
        /// 对象克隆
        /// </summary>
        /// <typeparam name="T">要克隆数据类型</typeparam>
        /// <param name="t">要克隆的对象</param>
        /// <returns>克隆后的对象</returns>
        public static T Clone<T>(T t)
        {
            return Deserialize<T>(Serialize(t));
        }

    }
    
}