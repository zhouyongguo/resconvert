﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LangApp.Api
{
    public class Language
    {

        public static string ToEnglish(string s)
        {
            //System.Threading.Thread.Sleep(500);
            string english = "";
            try
            {
                string serverUrl = @"http://fanyi.youdao.com/openapi.do?keyfrom=morninglight&key=1612199890&type=data&doctype=json&version=1.1&q="
                  + HttpUtility.UrlEncode(s);
                WebRequest request = WebRequest.Create(serverUrl);
                WebResponse response = request.GetResponse();
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    string str = reader.ReadToEnd();
                    translationData translation = translationData.Create(str);
                    english = translation.English();
                    reader.Close();
                }
                return english;
            }
            catch
            {
                return s;
            }

        }

        /// <summary>
        /// 将字符串转换为简体中文
        /// </summary>
        public static string ToSimplifiedChinese(string s)
        {
            return Strings.StrConv(s, VbStrConv.SimplifiedChinese, 0);
        }

        /// <summary>
        /// 将字符串转换为繁体中文
        /// </summary>
        public static string ToTraditionalChinese(string s)
        {
            return Strings.StrConv(s, VbStrConv.TraditionalChinese, 0);
        }
    }
}
