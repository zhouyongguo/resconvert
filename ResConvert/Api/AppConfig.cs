﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api
{
    public class AppConfig
    {

        static AppConfig _config = null;

        Dictionary<string, string> words = null;
        public string ProjectDir {set;get;}
        public int StrNumber { set; get; }
        public bool AutoTranslate { set; get; }
        public Dictionary<string, string> Word
        {
            get { 
                if(words==null)
                    words = new Dictionary<string, string>();
                return words;
            }
        }
        public AppConfig()
        {
            StrNumber=0;
            AutoTranslate = false;
        }
        public void AddWord(string key,string val)
        {
            if (string.IsNullOrEmpty(key))
                return;
            if (Word.ContainsKey(key))
                return;
            else {
                Word.Add(key, val);
            }            
         
        }



        public static AppConfig CreateConfig()
        {
            if (_config == null)
                _config = new AppConfig();
            return _config;
        }

        public bool ValidateDir(string dirName)
        {
            bool isNot = true;
            var ext = dirName.ToLower();
            switch (ext)
            {
                case "app_globalresources":
                case "bin":
                case "obj":
                case "properties":
                case "app_localresources":
                case ".git":
                case "app_code":
                case "app_data":
                    isNot= false;
                    break;
                default:
                    break;
            }
            return isNot;
        }

    }
}
