﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api
{
    public class LanguageRes
    {
        AppConfig config = AppConfig.CreateConfig();
        System.Xml.XmlDocument doc = null;
        string resDir = "";
        string resType = "en";//zh-hant zh-cn
        public string ResPath
        {
            get
            {

                if (resType == "en")
                    return System.IO.Path.Combine(resDir + "\\Res.en.resx");
                else if (resType == "zh-hant")
                    return System.IO.Path.Combine(resDir + "\\Res.zh-hant.resx");
                else if (resType == "zh-cn")
                    return System.IO.Path.Combine(resDir + "\\Res.zh-cn.resx");
                else
                    return System.IO.Path.Combine(resDir + "\\Res.resx");
            }

        }
        public LanguageRes(string dirPath, string langType)
        {
            resDir = dirPath;
            resType = langType;
            doc = new System.Xml.XmlDocument();

            if (System.IO.File.Exists(ResPath))
                doc.Load(ResPath);
            else
                doc.Load("res.resx");
        }

        public void Add(string key,string val)
        {
            var root = doc.SelectSingleNode("/root");
            var node = doc.SelectSingleNode("//data[@name='" + key + "']");
            if (node == null)
            {
                var item = doc.CreateElement("data");
                item.SetAttribute("name", key);
                item.SetAttribute("xml:space", "preserve");
                var valitem = doc.CreateElement("value");
                valitem.InnerText = getValue(val);
                item.AppendChild(valitem);
                root.AppendChild(item);
            }
        }

        private string getValue(string val)
        {
            if (resType == "en")
            {
                if (config.AutoTranslate && val.Length<100)
                    return Language.ToEnglish(val);
                else
                    return val;
            }
            else if (resType == "zh-cn")
                return Language.ToSimplifiedChinese(val);
            else if (resType == "zh-hant")
                return Language.ToTraditionalChinese(val);
            else
                return val;
        }
        public void Save()
        {
           
            foreach (string key in config.Word.Keys)
            {
                Add(key,config.Word[key]);
            }
            doc.Save(ResPath);
        }
    }
}