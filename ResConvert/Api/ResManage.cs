﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LangApp.Api
{
    public class ResManage
    {
        public ResManage()
        {
        }

       
        public void Save()
        {
            var config = AppConfig.CreateConfig();
            string rsDir = System.IO.Path.Combine(config.ProjectDir, "App_GlobalResources");
            if (Directory.Exists(rsDir) == false)
            {
                Directory.CreateDirectory(rsDir);
            }

            LanguageRes cn = new LanguageRes(rsDir, "zh-cn");
            cn.Save();

            LanguageRes tw = new LanguageRes(rsDir, "zh-hant");
            tw.Save();

            LanguageRes en = new LanguageRes(rsDir, "en");
            en.Save(); 
            
            LanguageRes res = new LanguageRes(rsDir, "");
            res.Save();
          
        }

    }
}
