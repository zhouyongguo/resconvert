﻿using LangApp.Api.Con;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LangApp.Api
{
    public class CsFile : FileBase
    {
        public CsFile(string filePath)
        {
            this.Path = filePath;
        }
        public override List<ConBase> GetCons()
        {
            List<ConBase> cons = new List<ConBase>();
            cons.Add(new CsResCon());
            cons.Add(new CsStrsCon());
            cons.Add(new CsStrCon());
            return cons;
        }
       
    }
}
